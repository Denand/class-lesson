class Fraction:
    n, d
    def __init__ (self, n, d):
        self.n = n; self.d = d
    def __str__ (self):
        return "%d/%d" % (self.n, self.d)
    def simplify(self):
        pass
    def __eq__ (self, f):
        n1 = self.n; n2 = f.n
        d1 = self.d; d2 = f.d
        return self.n1 == n2 and d1 == d2
    def __ne__ (self, f):
        return not self == f
    def __lt__ (self, f):
        return self.n < f.n and self.d < f.d
    def __gt__ (self, f):
        return self.n > f.n and self.d > f.d
    def __le__ (self, f):
        return self.n <= f.n and self.d <= f.d
    def __ge__ (self, f):
        return self.n >= f.n and self.d >= f.d
    def mul (self, f):
        self.n *= f.n
        self.d *= f.d
        self.simplify()
    def __mul__ (self, f):
        return Fraction (self.n * f.n, self.d * f.d)
    def __div__ (self, f):
        return Fraction (self.n % f.n, self.d % f.d)
    
a = Fraction(1,2); b = Fraction(1,3)
c = a * b
print(a * b)
